#!/usr/bin/perl

use strict;
use warnings;
use ASR;
use POSIX qw/strftime/;
use Carp qw/cluck/;
$SIG{__WARN__} = sub { cluck $_[0] };

my $ctx = ASR::Ladder->new;

$ctx->header('Player Info');

my $prec = $ctx->db_selectrow("SELECT * FROM player_info WHERE id = ?", {}, $ctx->param('pid'));
if (not $prec) {
	print $ctx->p("Player not found.");
	$ctx->footer;
	exit;
}

print $ctx->p("Player <strong>$prec->{nick}</strong> ($prec->{score}; $prec->{mgames} games this month) with id $prec->{id}. KGS games last refreshed on $prec->{last_update}");

print <<EOT;
<h2>Ladder Games List</h2>
<table id="gamelist">
<tr><th>White</th><th>Black</th><th>Date</th><th>Score\&Delta;</th></tr>
EOT
my $q = $ctx->{dbi}->prepare("SELECT * FROM player_game WHERE bid = ? OR wid = ?");
$q->execute($prec->{id}, $prec->{id});
while (my $r = $q->fetchrow_hashref) {
	my $class = ($r->{winid} == $prec->{id} ? 'winner' : 'loser');
	print "<tr class=\"$class\"><td>".$ctx->plink($r->{'wid'}, $r->{'wnick'})."</td><td>".$ctx->plink($r->{'bid'}, $r->{'bnick'})."</td><td><a href=\"$r->{url}\">$r->{date}</a></td><td>".($class eq 'winner' ? '+'.$r->{scoredelta} : 0)."</td></tr>\n";
}
print "</table>\n";

$ctx->footer;
