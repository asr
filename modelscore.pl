#!/usr/bin/perl

use strict;
use warnings;
use ASR;
use POSIX qw/strftime/;
use Carp qw/cluck/;
$SIG{__WARN__} = sub { cluck $_[0] };

my $ctx = ASR::Ladder->new;

$ctx->header('Score Change Projection');

my $p1rec = $ctx->db_selectrow("SELECT * FROM player WHERE nick = ?", {}, $ctx->param('p1'));
my $p2rec = $ctx->db_selectrow("SELECT * FROM player WHERE nick = ?", {}, $ctx->param('p2'));

my $err = 0;
if (not $p1rec) {
	print $ctx->p("Player '".$ctx->param('p1')."' not found.");
	$err++;
}
if (not $p2rec) {
	print $ctx->p("Player '".$ctx->param('p2')."' not found.");
	$err++;
}
if ($err) {
	$ctx->footer;
	exit;
}

my $p1proj = $ctx->recompute_score($p1rec->{score}, $p2rec->{score});
my $p2proj = $ctx->recompute_score($p2rec->{score}, $p1rec->{score});

my $p1l = $ctx->plink($p1rec->{id}, $p1rec->{nick});
my $p2l = $ctx->plink($p2rec->{id}, $p2rec->{nick});

print $ctx->p("Assuming game $p1l ($p1rec->{score}) vs $p2l ($p2rec->{score}):");
print <<EOT;
<table id="scoreprojection">
<tr><th>Winner</th><th>$p1rec->{nick}</th><th>$p2rec->{nick}</th></tr>
<tr><td>$p1rec->{nick}</td><td>$p1proj</td><td>$p2rec->{score}</td></tr>
<tr><td>$p2rec->{nick}</td><td>$p1rec->{score}</td><td>$p2proj</td></tr>
</table>
EOT

$ctx->footer;
