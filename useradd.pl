#!/usr/bin/perl

use strict;
use warnings;
use ASR;
use POSIX qw/strftime/;
use Carp qw/cluck/;
$SIG{__WARN__} = sub { cluck $_[0] };

my $ctx = ASR::Ladder->new;

$ctx->header('Adding user');

if ($ctx->param('pwd') ne $ctx->{adminpw}) {
	print $ctx->p('Invalid password.');
	$ctx->footer;
	exit;
}

if ($ctx->db_do('INSERT INTO player SET nick=?', {}, $ctx->param('nick'))) {
	print $ctx->p('KGS nick '.$ctx->param('nick').' added.')
} else {
	print $ctx->p('Insert failed.');
}

$ctx->footer;
