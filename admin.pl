#!/usr/bin/perl

use strict;
use warnings;
use ASR;
use POSIX qw/strftime/;
use Carp qw/cluck/;
$SIG{__WARN__} = sub { cluck $_[0] };

my $ctx = ASR::Ladder->new;

$ctx->header();

print <<EOT;

<div id="useradd">
<h2>Add User</h2>
<form action="useradd.pl" method="post">
<p>Admin pw: <input type="password" name="pwd"> <br/>
KGS nick: <input type="text" name="nick">
<input type="submit" name="s" value="Add user"></p>
</form>
</div>

<div id="reset">
<h2>Monthly Ladder Reset</h2>
<form action="reset.pl" method="post">
<p>Admin pw: <input type="password" name="pwd"> <br/>
Delete players with no games after date (YYYY-MM-DD): <input type="text" name="cutoffdate">
<input type="submit" name="s" value="Do it"></p>
</form>
</div>

EOT

$ctx->footer;
