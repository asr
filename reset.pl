#!/usr/bin/perl

use strict;
use warnings;
use ASR;
use POSIX qw/strftime/;
use Carp qw/cluck/;
$SIG{__WARN__} = sub { cluck $_[0] };

my $ctx = ASR::Ladder->new;

$ctx->header('Monthly reset');

if ($ctx->param('pwd') ne $ctx->{adminpw}) {
	print $ctx->p('Invalid password.');
	$ctx->footer;
	exit;
}

=can trigger infinite loop :/
if ((system('mysqldump -u asr -p'.$ctx->{password}.' asr >/tmp/ASR-`date +%F`.dump') >> 8) != 0) {
	print $ctx->p("Error: Database backup failed: $?, $!");
	$ctx->footer;
	exit;
}
=cut

($ctx->db_do('START TRANSACTION') and
	$ctx->db_do('CREATE TEMPORARY TABLE inactive (id INT)') and
	$ctx->db_do('INSERT INTO inactive SELECT id FROM raw_ladder ' .
			'WHERE last_game IS NULL OR last_game < ' .
			'DATE("'.$ctx->param('cutoffdate').'")') and
	$ctx->db_do('DELETE FROM player WHERE id IN (SELECT id FROM inactive)') and
	$ctx->db_do('DROP TABLE inactive') and
	$ctx->db_do('COMMIT')) or
		print $ctx->p('Removing inactive players failed.');

$ctx->db_do('UPDATE player SET score = 100 WHERE score < 100') or
	print $ctx->p('Flooring score to 100 failed.');
$ctx->db_do('UPDATE player SET score = 200 WHERE score > 200') or
	print $ctx->p('Ceiling score to 200 failed.');
$ctx->db_do('UPDATE player SET score = 100 + (score - 100) / 2 WHERE score > 100') or
	print $ctx->p('Halving score failed.');

$ctx->footer;
