#!/usr/bin/perl

use strict;
use warnings;
use ASR;
use POSIX qw/strftime/;
use Carp qw/cluck/;
$SIG{__WARN__} = sub { cluck $_[0] };

my $ctx = ASR::Ladder->new;

$ctx->header();

print "<div id=\"ladder\"><h2>Blitz Ladder Listing</h2>\n";
print "<p id=\"skiptable\"><a href=\"#instructions\">Go to instructions</a></p>\n";

print "<table><tr><th>#</th><th>Player</th><th>Score</th><th>Win</th><th>Loss</th><th>Ratio</th><th>g.t.m.</th><th>Last Game</th><th>...score&Delta;</th><th>Streak</th></tr>\n";
my $q = $ctx->{dbi}->prepare("SELECT * FROM ladder");
$q->execute();
my $i = 1;
while (my $r = $q->fetchrow_arrayref) {
	my $id = $r->[0];
	$r->[0] = $i++;
	$r->[1] = $ctx->plink($id, $r->[1]);
	print "<tr><!--$id--><td>".join("</td><td>", @$r)."</td></tr>\n";
}
print "</table></div>\n";

print <<EOT;

<div id="extrastuff">

<div id="instructions">
<h2>How It Works</h2>
<p>New ladder games are automatically checked every hour (at :54); you do not need to report them to anybody. A ladder game must be <strong>1:00 + <i>n</i>&times;0:10</strong> (<i>n</i> is 1 to 5), type <i>{free, ranked or simul}</i>, longer than 30 moves, both players must be ladder members and one of the players must say <strong>'ASR ladder'</strong> during the first 30 moves. Board size, handicap, komi, ruleset, adding time later and granting undo is choice of players and does not affect the scoring!</p>
<p>To join the ladder, just leave pasky (pasky\@ucw.cz) a message. New ladder players start at 100 points, and for each game they win they get <tt>((score<sub>loser</sub> - score<sub>winner</sub>) / $ctx->{scoring}->{winportion})</tt> points, but at least $ctx->{scoring}->{winfloor} point per win. No points are deducted for losses, but <em>all</em> players lose $ctx->{scoring}->{dailydrop} points per day (though they never go below 100). Players who do not play any games for a month are dropped from the ladder.</p>

<h2>Prize Rules</h2>
<p>In case a prize is announced for some month (see ASR room message), on the end of the month, score is normalized: people who played no games in that month are removed from the ladder, score less than 100 is reset to 100, score more than 200 is capped to 200 and for all score over 100, the amount over 100 is halved. A person must have played at least 25 games to be eligible for a prize. Also, one person cannot win the first prize twice in a row.</p>
</div>

<div id="scoremodel">
<h2>Score Change Modelling</h2>
<form action="modelscore.pl" method="post">
<p>Game of <input type="text" name="p1"> vs <input type="text" name="p2"> <input type="submit" name="s" value="Project score change"></p>
</form>
</div>

<p id="adminlink"><a href="admin.pl">Admin interface</a></p>
<p id="sourcelink"><a href="http://repo.or.cz/w/asr.git">Source code</a></p>

</div>

EOT

$ctx->footer;
