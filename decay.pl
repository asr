#!/usr/bin/perl

use strict;
use warnings;
use lib qw(/home/pasky/WWW/asr);
use ASR;
use POSIX qw/strftime/;
use Carp qw/cluck/;
$SIG{__WARN__} = sub { cluck $_[0] };

my $ctx = ASR::Ladder->new;

$ctx->db_do('UPDATE player SET score = score-'.$ctx->{scoring}->{dailydrop}.' WHERE score > 100');
