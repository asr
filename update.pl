#!/usr/bin/perl
use strict;
use warnings;
use Time::Local;
use POSIX qw/strftime/;
use LWP::UserAgent;
use lib qw(/home/pasky/WWW/asr);
use ASR;

our $ctx = ASR::Ladder->new;

our $delay = 5;

our ($lwp_response, $page_fail);

$|=1; # Turn off output buffering.


our ($csec, $cmin, $chr, $cday, $cmon, $cyear) = gmtime();
$cyear += 1900;


print "\n+++ Starting update job at $cyear-$cmon-$cday $chr:$cmin:$csec\n\n";


# Check games of new players one week back. We should do global updates way
# more often than that anyway.
our $deftime = time() - 7*86400;

# ======================
# Decide what to update!
# ======================

my $mode = $ARGV[0] || "all";

my @games;
our ($player, $uall) = ({}, 0);
if ($mode eq 'all') {
	$delay = $delay || 10;
	@games = allgames();
	$uall = 1;

} elsif ($mode eq 'player') {
	$player = $ctx->db_selectrow("SELECT *, UNIX_TIMESTAMP(last_game) AS last_game FROM player_last_game WHERE nick = ?", {}, $ARGV[1]);
	if ($player) {
		@games = playergames($player);
	} else {
		print $ctx->p("Invalid player nick: ".$ARGV[1]);
	}
}

@games = sort { $a->{time} <=> $b->{time} } @games;
print $ctx->p("I have ".scalar(@games)." game(s) to investigate.\n");

# For each potentially new game, run the game parser on it.
foreach my $game (@games) {
	parsegame($game);
}


# =================================================
# Helper routines to bulk update many KGS Usernames
# =================================================

sub allgames {
	my @update;
	my $q = $ctx->{dbi}->prepare("SELECT *, UNIX_TIMESTAMP(last_game) AS last_game FROM player_last_game ORDER BY last_update ASC");
	$q->execute();
	while (my $r = $q->fetchrow_hashref) {
		push @update, playergames($r);
	}
	return @update;
}

# =================================
# Update an individual KGS Username
# =================================

sub playergames {
	my ($player) = @_;
	print $ctx->p("Updating KGS user name $player->{nick}.\n");
	my $rank;

	# Has the user been updated recently?
	my $time = $player->{last_game} || $deftime;
	#if (time() - ($player->{last_update} || 0) < 3600) {
	#	print $ctx->p("Player $player->{nick} was updated too recently; skipping.\n");
	#	return;
	#}

	# Convert time to a format we can plug into the archives.
	my ($sec,$min,$hr,$day,$mon,$year) = gmtime $time;
	$year += 1900;

	# Now get a list of all games spanning all months since the last update.
	# We go back 5 hours before the last game in case we missed an ongoing
	# game.
	undef $page_fail;
	my @games;
	while(($year < $cyear) || ($year == $cyear && $mon <= $cmon)) {
		push @games, getnewgames($year, $mon + 1, $player->{nick}, $time - 3600 * 5, \$rank);
		# In case a game was playing during prev update, subtract 5 hours.
		$mon++;
		if ($mon>11) {
			$mon = 0;
			$year++;
		}
	}
	# Finally, use the rank we found from the system to update the player.
	if ($page_fail) {
		$ctx->db_do("UPDATE player SET last_update = NOW() WHERE id = ?", {}, $player->{pid});
	} else {
		$ctx->db_do("UPDATE player SET rank = ?, last_update = NOW() WHERE id = ?", {}, $rank, $player->{pid});
	}
	
	return @games;
}

# ====================================================================
# Get a list of games from the KGS Archives page of a given user/month
# ====================================================================

# $time is the time of the oldest game to include. $rankref will be updated
# with the most recent rank.

sub getnewgames {
	my ($year, $month, $user, $time, $rankref) = @_;
	print $ctx->p("Getting page http://www.gokgs.com/gameArchives.jsp?user=$user&year=$year&month=$month...\n");
	$_ = get("http://www.gokgs.com/gameArchives.jsp?user=$user&year=$year&month=$month");
	if (!$_) {
		print $ctx->p("Failed to get page: ".$lwp_response->status_line);
		$page_fail = 1;
		sleep($delay) if $delay;
		return;
	}
	sleep($delay) if $delay;
	my @games;
	my $first = 1;
	while (
		m#<tr><td><a href="([^"]*)">Yes</a></td><td><a[^>]*>(\S+(?:\s+\[\S+\])?)(?:</a><br/?><a[^>]*>(\S+(?:\s+\[\S+\])?))?</a></td><td><a[^>]*>(\S+(?:\s+\[\S+\])?)(?:</a><br/?><a[^>]*>(\S+(?:\s+\[\S+\])?))?</a></td><td>19[^<]*19[^<]*</td><td>(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)\s+([AP])M</td><td>(?:Free|Ranked|Simul)</td><td>([BW])\+[^<]*?</td></tr>#gs) {
		# Order:
		# URL
		# W player 1
		# (W player 2)
		# B player 1
		# (B player 2)
		# D
		# M
		# Y
		# H
		# M
		# AM/PM
		# Winner
		#print $ctx->p("Test: $1 $2 $3 $4 $5 $6 $7 $8 $9 $10\n");
		my $ctime = timeval($6,$7,$8,$9,$10,$11);

		my $url = $1;
		my $quser = quotemeta $user;
		my ($white, $black) = ($2, $4);
		my $won = $12;
		my ($wrank, $brank);
		$wrank = rank($1) if $white =~ s/\s+\[(.*)\]//;
		$brank = rank($1) if $black =~ s/\s+\[(.*)\]//;

		$$rankref = lc $user eq lc $white ? $wrank : $brank if $first;
		undef $first;

		if ($ctime < $time) {
			print $ctx->p("Discarding $url (and anything after) since it's dated before update threshold (".strftime("%H:%M %d/%m/%Y", gmtime $time).").\n");
			last;
		}

		push @games, { url => $url, time => $ctime };

		print $ctx->p("Potential new ladder game: $url - ".strftime("%H:%M %d/%m/%Y", gmtime $ctime)."\n");
	}
	return @games;
}

# Convert kyu / dan into decimal rank.
sub rank {
	return $1 if $_[0] =~ /(\d+)k/i;
	return 1 - $1 if $_[0] =~ /(\d+)d/i;
	return;
}

# =========================================================================
# Investigate a game found in the archives to establish if it's a clan game
# =========================================================================

sub parsegame {
	my ($game) = @_;

	print $ctx->p("Investigating game: $game->{url}\n");

	$ctx->db_do("START TRANSACTION");

	# Check if the game isn't being re-parsed...

	my $r = $ctx->db_selectone("SELECT id FROM game WHERE url = ?", {}, $game->{url});
	if ($r) {
		# Error message not really an error.
		print $ctx->p("I've already seen this game...\n");
		return;
	}


	my $sgf = get($game->{url});
	sleep(1) if $delay; # Don't need to wait 10 secs for these...

	{
		my $zurl = $game->{url}; $zurl =~ s#/#_#g;
		my $f;
		if (open $f, ">/tmp/asrgames/$zurl") {
			print $f $sgf;
			close $f;
		}
	}


	# Get all of the info we're interested in out of the SGF file.

	my $komi = 0;
	$komi = $1 if ($sgf =~ /KM\[(-?\d+(?:\.\d+)?)\]/);
	my $handi = 0;
	$handi = $1 if ($sgf =~ /HA\[(\d+)\]/);
	$handi = 1 if $handi == 0 && $komi < 1;

	my $white = 'unknown';
	$white = $1 if ($sgf =~ /PW\[([^\]]+)\]/);
	my $black = 'unknown';
	$black = $1 if ($sgf =~ /PB\[([^\]]+)\]/);

	my (@white_decision, @black_decision);
	my ($result, $result_by) = (0, "Unknown");
	if ($sgf =~ /RE\[([^\]]+)\]/) {
		my $temp = $1;
		if (lc $temp eq 'jigo') {
			$result = 0;
		} elsif ($temp =~ /([BW])+(.*)/) {
			$result = ($1 eq 'B') ? 1 : -1;
			$result_by = $2;
		}
	}

	$ctx->db_do("INSERT INTO game SET winner = ?, url = ?", {}, $result > 0 ? 'black' : 'white', $game->{url});
	$game->{id} = $ctx->lastid;

	# From now on we can check if this is ladder game:

	my $whiterec = $ctx->db_selectrow("SELECT * FROM player WHERE nick = ?", {}, $white);
	my $blackrec = $ctx->db_selectrow("SELECT * FROM player WHERE nick = ?", {}, $black);
	if (not defined $whiterec or not defined $blackrec) {
		print $ctx->p("This is not a ladder game...\n");
		return;
	}

	my $rules = "";
	#if ($sgf !~ /RU\[Japanese\]/) {
	if ($sgf =~ /RU\[([^\]]+)\]/) {
#		print $ctx->p("Not a ladder game: Bad ruleset.\n");
		$rules .= "RU[$1]";
	}
	if ($sgf !~ /SZ\[([^\]]+)\]/) {
#		print $ctx->p("Not a ladder game: Bad board size.\n");
		$rules .= "SZ[$1]";
	}
	if ($sgf !~ /TM\[(0|[1-9]|[1-5]\d|60)\]OT\[[1-5]x10 byo-yomi\]/) {
		print $ctx->p("Not a ladder game: Bad time settings.\n");
		return;
	}
	#if ($sgf =~ /TM\[([^\]]+)\]/) {
	#	$rules .= "TM[$1]";
	#}
	#if ($sgf =~ /OT\[([^\]]+)\]/ ) {
	#	$rules .= "OT[$1]";
	#}
	print $ctx->p("Fits ladder settings.\n");

	my @m = ($sgf =~ /([BW]L\[\d+(?:\.\d+)?\])/g);
	my $moves = @m;
	if ($moves < 30) {
		print $ctx->p("Not a ladder game: Only $moves moves (needs 30).\n");
		return;
	}

	my $tagmoves = is_ladder($sgf, $black, $white);
	if (!defined $tagmoves) {
		print $ctx->p("Is not a ladder game: No tag.\n");
		return;
	} elsif ($tagmoves > 30) {
		print $ctx->p("Is not a ladder game: Tag said too late (after move $tagmoves).\n");
		return;
	} else {
		print $ctx->p("Tag OK: Said after move $tagmoves.\n");
	}

	# Now perform the necessary database updates.

	my $black_score = $blackrec->{score};
	my $white_score = $whiterec->{score};
	if ($result > 0) {
		$black_score = $ctx->recompute_score($black_score, $white_score);
		$ctx->db_do("UPDATE player SET score = ? WHERE id = ?", {}, $black_score, $blackrec->{id});
	} else {
		$white_score = $ctx->recompute_score($white_score, $black_score);
		$ctx->db_do("UPDATE player SET score = ? WHERE id = ?", {}, $white_score, $whiterec->{id});
	}

	$ctx->db_do("INSERT INTO ladder_game SET id = ?, black = ?, white = ?, black_score_old = ?, white_score_old = ?, black_score_new = ?, white_score_new = ?", {}, $game->{id}, $blackrec->{id}, $whiterec->{id}, $blackrec->{score}, $whiterec->{score}, $black_score, $white_score);

	$ctx->db_do("COMMIT");
}

# Ensure that the ladder tag was said; returns moves before tag or undef.

sub is_ladder {
	my ($sgf, $black, $white) = @_;
	if ($sgf =~ /^(.*?)($black|$white)\s+\[[^\\]*\\\]:[^\n]*\b(?i:(?:ASR *ladd*er|add you to the ladder))\b/s) {
		my @m = ($1 =~ /([BW]L\[\d+(?:\.\d+)?\])/g);
		my $moves = @m;
		return $moves;
	}
	return;
}

sub timeval {
	my ($mo, $d, $y, $h, $mi, $p) = ($_[0] - 1, $_[1], $_[2]+2000, $_[3], $_[4], $_[5] eq 'P');
	if ($p && $h < 12) {
		$h += 12;
	} elsif (!$p && $h == 12) {
		$h = 0;
	}
	return timegm(0, $mi, $h, $d, $mo, $y);
}

sub get {
	my $ua = LWP::UserAgent->new;
	$ua->timeout(10);
	$ua->env_proxy;

	$lwp_response = $ua->get($_[0]);

	if ($lwp_response->is_success) {
		return $lwp_response->content;  # or whatever
	}
	return undef;
}
