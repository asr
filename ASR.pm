package ASR::Ladder;

use strict;
use warnings;
use Time::Local;
use Time::HiRes;
use LWP::Simple;
use POSIX qw/strftime/;
use CGI;
use DBI;

$ENV{HOME} = "/home/pasky/WWW/asr";

sub new {
	my $this = {};
	bless $this;
	$this->{startup} = Time::HiRes::time();
	$this->{dbreqs} = 0;
	$this->{cgi} = new CGI;
	$this->{scoring} = {
		winportion => 3, # how big portion of score delta are win points
		winfloor => 2, # minimal number of win points
		dailydrop => 0.5, # daily global point drop
	};
	$this->get_dbi;
	return $this;
}

sub get_dbi {
	return $_[0]->{dbi} if exists $_[0]->{dbi};

	open MYCNF, "$ENV{HOME}/.my.cnf";
	local $/;
	my $contents = <MYCNF>;
	close MYCNF;
	my ($user, $database, $password);
	$user = $1 if $contents =~ /user = (.*)/;
	$database = $1 if $contents =~ /database = (.*)/;
	$password = $1 if $contents =~ /password = (.*)/;
	$_[0]->{adminpw} = $1 if $contents =~ /adminpw = (.*)/;

	if (!$user || !$database || !$password || !$_[0]->{adminpw}) {
		&die_clean_fatal("Sorry, the .my.cnf file appears to be corrupt");
	}

	$_[0]->{dbi} = DBI->connect("dbi:mysql:database=$database", $user, $password);
	$_[0]->{password} = $password;

	if (!$_[0]->{dbi}) {
		$_[0]->die_fatal_db("Sorry, I can't seem to connect to the database.");
	}

	return $_[0]->{dbi};
}

sub die_fatal {
	$_[0]->header("Argh") unless $_[0]->{header};
	print $_[0]->h2("Fatal error: $_[1]");
	print $_[0]->h3("$_[2]");
	$_[0]->footer;
	exit;
}

sub die_fatal_db {
	$_[0]->die_fatal($_[1], "Database says: ".DBI->errstr);
}

sub die_fatal_permissions {
	$_[0]->die_fatal($_[1], "You don't have permission to do that!");
}

sub die_fatal_badinput {
	$_[0]->die_fatal($_[1], "Your input incomplete/invalid.");
}

sub header {
	my $this = $_[0];
	&die_clean_fatal("Header output twice?!") if $this->{header};
	$this->{header} = 1;
	my $cgi = new CGI;
	print $cgi->header;
	print $cgi->start_html(-title => "ASR Ladder", -style => "style.css");
	print $cgi->h1("ASR Ladder BETA");

	#print qq<<p class=nav><a href="?mode=index$period">Summary</a> | <a href="?mode=stats$period">Stats</a> | <a href="?mode=brawl$period">Brawl</a> | <a href="?mode=votes">Votes</a></p>>;

	$_[1] and print $cgi->h2($_[1]);
	$this->{cgi} = $cgi;
	return $cgi;
}

sub action_fail {
	print $_[0]{cgi}->h2("Failed");
	print $_[0]{cgi}->p($_[1]);
}

sub action_success {
	print $_[0]{cgi}->h2("Success");
	print $_[0]{cgi}->p($_[1]);
}

sub footer {
	my $this = shift;
	my $cgi = $this->{cgi};
	my $time = Time::HiRes::time() - $this->{startup};
	#print $cgi->p("Request took $time secs, made $this->{dbreqs} queries.");
	print $cgi->end_html;
}

sub db_do {
	my $this = shift @_;
	$this->{dbreqs}++;
	return $this->{dbi}->do(@_);
}

sub db_select {
	my $this = shift @_;
	$this->{dbreqs}++;
	return $this->{dbi}->selectall_hashref(@_);
}

sub db_selectrow {
	my $this = shift @_;
	$this->{dbreqs}++;
	return $this->{dbi}->selectrow_hashref(@_);
}

sub db_selectone {
	my $this = shift @_;
	my $row = $this->{dbi}->selectrow_arrayref(@_);
	$this->{dbreqs}++;
	return undef unless $row;
	return $row->[0];
}

sub lastid {
	my $this = shift @_;
	return $this->db_selectone("SELECT LAST_INSERT_ID();");
}

sub baseurl {
	my $this = shift;
	my %vals = $this->{cgi}->Vars();
	delete $vals{$_} foreach (@_);
	if (keys %vals) {
		return '?'.(join '&amp;', map { "$_=$vals{$_}" } keys %vals).'&amp;';
	} else {
		return '?';
	}
}

sub hidden {
	return qq|<input type="hidden" name="$_[1]" value="$_[2]"/>|;
}

sub textarea {
	my $this = shift;
	return $this->{cgi}->textarea(@_);
}

sub textfield {
	my $this = shift;
	return $this->{cgi}->textfield(@_);
}

sub textfield_ {
	return qq|<input type="text" name="$_[1]" value="$_[2]" size="$_[3]" maxlength="$_[4]"/>|;
}

sub passfield {
	my $this = shift;
	return $this->{cgi}->password_field(@_);
}

sub param {
	my $this = shift;
	return $this->{cgi}->param(@_);
}

sub h1 {
	my $this = shift;
	return $this->{cgi}->h1(@_);
}

sub h2 {
	my $this = shift;
	return $this->{cgi}->h2(@_);
}

sub h3 {
	my $this = shift;
	return $this->{cgi}->h3(@_);
}

sub h4 {
	my $this = shift;
	return $this->{cgi}->h4(@_);
}

sub p {
	my $this = shift;
	return $this->{cgi}->p(@_);
}

sub submit {
	my $this = shift;
	return $this->{cgi}->submit(@_);
}

sub end_form {
	my $this = shift;
	return $this->{cgi}->end_form(@_);
}

sub escapeHTML {
	my $this = shift;
	return $this->{cgi}->escapeHTML(@_);
}

sub begin_form {
	my $this = shift;
	if (shift eq 'get') {
		print $this->{cgi}->start_form(-method => 'GET', -action => '?');
	} else {
		print $this->{cgi}->start_form(-method => 'POST', -action => 'index.pl');
	}
	while (my $name = shift) {
		my $val = shift;
		print $this->hidden($name, $val);
	}
}

sub list {
	my $this = shift;
	print "<ul>";
	print "<li>$_</li>" foreach(@_);
	print "</ul>"
}

sub plink {
	my $this = shift;
	my ($id, $nick) = @_;
	return "<a href=\"player.pl?pid=$id\" class=\"playerlink\">$nick</a>";
}

sub recompute_score {
	my $this = shift;
	my ($winner, $loser) = @_;

	# Fine-tunable scoring constants
	my $gameval = 1; # points per completely even game

	my $delta = 0.0 + $loser - $winner;
	my $extra = $delta / $this->{scoring}->{winportion};
	($extra > $this->{scoring}->{winfloor}) or $extra = $this->{scoring}->{winfloor};
	return $winner + $extra;
}
